import express from 'express';
import * as productos from './productos';

const app = express();
app.use(express.json());

app.get('/',function(request, response){
    response.send('Hola mundo!!!');
    
});

app.get('/productos',function(request,response){
    response.send(productos.getStock())
});

app.post('/productos', function(request, response) {
    const body = request.body
     response.send(productos.storeProductos(body))
 });

app.delete('/productos/:id' , function(req, response) {
    const idProducto = Number(req.params.id);
    
    if (idProducto>0 ){
        response.send(productos.deleteProductos(idProducto))
    }else{
        response.send(req.params.id);
    }
    
});


app.listen(3000,function(){
    console.log('Servidor escuchando en http://localhost:3000');
    
})
